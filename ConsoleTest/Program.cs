﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.IO;
using SnapshotService;

namespace ConsoleTest
{
    class Program
    {
        private static readonly object Locker = new object();
        static void Main(string[] args)
        {
            Thread.CurrentThread.Name = "main";

            List<string> urlList;
            string inputFile = @"C:\out\urls.txt";

            // Get some input
            if (File.Exists(inputFile))
            {
                urlList = new List<string>(File.ReadLines(inputFile));
            }
            else
            {
                urlList = new List<string>
                {
                    "http://www.aftonbladet.se/",
                    "https://en.wikipedia.org/wiki/Nava_de_la_Asunci%C3%B3n",
                    "http://www.facebook.com/",
                    "https://en.wikipedia.org/wiki/Nigahen:_Nagina_Part_II",
                    "http://www.dn.se/",
                    "https://msdn.microsoft.com/",
                    "http://www.expressen.se/",
                    "https://en.wikipedia.org/wiki/Spamware",
                    "https://msdn.microsoft.com/en-us/library/system.web.mvc.selectlist.selectlist(v=vs.118).aspx#M:System.Web.Mvc.SelectList.",
                    "http://www.detectify.com/",
                    "http://www.svd.se/",
                    "http://reducer.io/",
                    "https://en.wikipedia.org/wiki/Minimum_spanning_tree",
                };
            }

            // Set up queue workers will read data from
            var queue = new ConcurrentQueue<Uri>();
            int workerCount = 3;

            //Start worker threads
            for (int i = 0; i < workerCount; i++)
            {
                int workerId = i;
                var thread = new Thread(() =>
                {
                    var s = new Snapshot();
                    s.Start(workerId, queue);
                });

                thread.Start();
            }

            // Main thread sleeps for 5 seconds before adding work
            Console.WriteLine("[main] Main thread sleeping");
            Thread.Sleep(5000);

            // Add some work
            Console.WriteLine("[main] Adding jobs to work pool");
            foreach (string address in urlList)
            {
                var uri = new Uri(address);
                queue.Enqueue(uri);
            }
        }
    }
}
