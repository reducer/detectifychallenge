﻿using System;
using System.Threading;

namespace SnapshotService
{
    public static class Logger
    {
        public static void Log(string message)
        {
            Console.WriteLine($"[{Thread.CurrentThread.Name}] {message}");
        }
    }
}
