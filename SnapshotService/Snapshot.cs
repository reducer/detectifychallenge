﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Threading;
using System.IO;
using SnapshotService.Services;

namespace SnapshotService
{
    public class Snapshot
    {
        public string OutputDirectory { get; set; } = @"C:\out\";
        public IScreenshotService Service = new AlxWebScreenshotService();

        // Thread settings
        private const int ThreadIdleTime = 2000;

        public void Start(int workerId, ConcurrentQueue<Uri> addressQueue)
        {
            ThreadSetup(workerId);
            Logger.Log("Worker thread started");

            while (true)
            {
                bool working = Work(addressQueue);

                // Sleep if out of work, otherwise look for more work
                if (!working)
                {
                    Thread.Sleep(ThreadIdleTime);
                }
            }
        }

        private void ThreadSetup(int workerId)
        {
            Thread.CurrentThread.Name = $"worker{workerId}";
            OutputDirectory = $@"{OutputDirectory}{Thread.CurrentThread.Name}\";

            Directory.CreateDirectory(OutputDirectory);
        }

        public bool Work(ConcurrentQueue<Uri> addressQueue)
        {
            Uri address;

            if (!addressQueue.TryDequeue(out address))
            {
                Logger.Log("No work, sleeping");
                return false;
            }

            // Work received
            SaveScreenshot(address);

            return true;
        }

        private void SaveScreenshot(Uri address)
        {
            Logger.Log($"Saving from {address.Host}");

            // Use Unix epoch as timestamp in filename
            string epoch = DateTimeOffset.Now.ToUnixTimeSeconds().ToString();
            string fileName = $"{epoch}-{address.Host}.png";
            string filePath = $"{OutputDirectory}{fileName}";

            // Time execution, take screenshot
            Stopwatch sw = Stopwatch.StartNew();
            Service.Save(address, filePath);
            sw.Stop();

            Logger.Log($"Saved {address.Host} in {sw.ElapsedMilliseconds} ms");
        }
    }
}
