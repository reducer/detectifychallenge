﻿using System;
using System.Drawing.Imaging;
using Alx.Web;

namespace SnapshotService.Services
{
    public class AlxWebScreenshotService : IScreenshotService
    {
        // Image settings
        private Devices deviceFormat = Devices.Desktop;
        private ImageFormat imageFormat = ImageFormat.Png;

        public void Save(Uri address, string fileName)
        {
            Screenshot.Save(address.ToString(), fileName, imageFormat, deviceFormat);
        }
    }
}
