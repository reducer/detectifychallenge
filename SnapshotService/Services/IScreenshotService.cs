﻿using System;

namespace SnapshotService.Services
{
    public interface IScreenshotService
    {
        void Save(Uri address, string fileName);
    }
}
